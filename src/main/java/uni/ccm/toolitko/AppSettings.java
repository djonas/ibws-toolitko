package uni.ccm.toolitko;

/**
 * Class containing all vital configuration of ibws-tool application. Those,
 * that cannot be changed during runtime should be final.
 * 
 * @author Jonáš Obdržálek
 *
 */
public class AppSettings {
	// Arguments definition, i.e. each argument name.
	public static final String arg_inputDirectory = "inputDirectory";
	public static final String arg_outputDirectory = "outputDirectory";
	public static final String arg_forceTypeRebuild = "forceTypeRebuild";
	public static final String arg_forceOperationRebuild = "forceOperationRebuild";
	
	// Application Settings, which can be passed in main args
	public static boolean forceTypeRebuild = false;
	public static boolean forceOperationRebuild = false;
	public static String inputDirectory;
	public static String outputRootDirectory;
	public static String outputWsdlDirectory;
	public static String outputTypeDirectory;
	
	// Static application settings, like workable extensions, attributes to be excluded, etc...
	public static final String[] xmlExtensions = { "XML", "xml", "wsdl", "WSDL" };
	public static final String[] simpleTypes = { "int", "string", "boolean", "dateTime" };
	public static final String[] attributeParams = { "type", "ccmName", "format", "codeList", "description",
			"maxLength", "required", "metadata" };

}
