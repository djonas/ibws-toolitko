package uni.ccm.toolitko.processing.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import uni.ccm.toolitko.dto.AttrMetadataDTO;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;

public class SchemaMerger {

	public static ComplexTypeDTO mergeComplexTypes(ComplexTypeDTO newCt, ComplexTypeDTO oldCt) {
		Map<String, ComplexTypeAttrDTO> newCtAttrMap = initializeAttributeMap(newCt.getAttributes());
		Map<String, ComplexTypeAttrDTO> oldCtAttrMap = initializeAttributeMap(oldCt.getAttributes());
		oldCt.setAttributes(mergeAttributes(newCtAttrMap, oldCtAttrMap));
		return oldCt;
	}

	private static List<ComplexTypeAttrDTO> mergeAttributes(Map<String, ComplexTypeAttrDTO> newCtAttrMap,
			Map<String, ComplexTypeAttrDTO> oldCtAttrMap) {
		List<String> toDelete = new ArrayList<String>();
		for (Map.Entry<String, ComplexTypeAttrDTO> entry : oldCtAttrMap.entrySet()) {
			String key = entry.getKey();
			if (newCtAttrMap.containsKey(key)) {
				ComplexTypeAttrDTO newAttr = newCtAttrMap.get(key);
				ComplexTypeAttrDTO oldAttr = oldCtAttrMap.get(key);
				if (!newAttr.equals(oldAttr)) {
					oldCtAttrMap.put(key, newCtAttrMap.get(key));
				} else {
					oldAttr.setMetadata(mergeMetadata(newAttr.getMetadata(), oldAttr.getMetadata()));;
				}
				if (oldAttr.isComplexType()) {
					oldAttr.setAttributes(mergeAttributes(initializeAttributeMap(newAttr.getAttributes()),
							initializeAttributeMap(oldAttr.getAttributes())));
				}
				newCtAttrMap.remove(key);
			} else {
				toDelete.add(key);
			}
		}
		for (String key : toDelete) {
			newCtAttrMap.remove(key);
			oldCtAttrMap.remove(key);
		}
		for (Map.Entry<String, ComplexTypeAttrDTO> entry : newCtAttrMap.entrySet()) {
			oldCtAttrMap.put(entry.getKey(), entry.getValue());
		}

		ArrayList<ComplexTypeAttrDTO> mergedAttributes = new ArrayList<ComplexTypeAttrDTO>(oldCtAttrMap.values());
		return mergedAttributes;
	}

	private static AttrMetadataDTO mergeMetadata(AttrMetadataDTO newMetadata, AttrMetadataDTO oldMetadata) {
			oldMetadata.setCcmName(mergeMetadata(newMetadata.getCcmName(), oldMetadata.getCcmName()));
			oldMetadata.setCodeList(mergeMetadata(newMetadata.getCodeList(), oldMetadata.getCodeList()));
			oldMetadata.setDescription(mergeMetadata(newMetadata.getDescription(), oldMetadata.getDescription()));
			oldMetadata.setFormat(mergeMetadata(newMetadata.getFormat(), oldMetadata.getFormat()));
			oldMetadata.setRequired(mergeMetadata(newMetadata.getRequired(), oldMetadata.getRequired()));
			oldMetadata.setMaxLength(mergeMetadata(newMetadata.getMaxLength(), oldMetadata.getMaxLength()));
			return oldMetadata;
	}

	private static Map<String, ComplexTypeAttrDTO> initializeAttributeMap(List<ComplexTypeAttrDTO> attrs) {
		Map<String, ComplexTypeAttrDTO> typeMap = new HashMap<String, ComplexTypeAttrDTO>();
		if (attrs != null) {
			for (ComplexTypeAttrDTO attr : attrs) {
				typeMap.put(attr.getName(), attr);
			}
		}
		return typeMap;

	}
	
	private static boolean isNullOrEmpty(String string){
		return string == null || StringUtils.EMPTY.equals(string) || string.equals("-") || string.equals("?");
	}
	
	private static String mergeMetadata(String newVal, String oldVal){
		if( isNullOrEmpty(oldVal) || (!isNullOrEmpty(newVal) && !oldVal.equals(newVal))){
			return newVal;
		} else {
			return oldVal;
		}
	}
	
	
	private static int mergeMetadata(int newVal, int oldVal){
		if(oldVal != newVal){
			return newVal;
		} else {
		    return oldVal;
		}
	}

}
