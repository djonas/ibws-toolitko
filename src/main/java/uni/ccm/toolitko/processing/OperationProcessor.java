package uni.ccm.toolitko.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import com.predic8.wsdl.AbstractPortTypeMessage;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.Operation;
import com.predic8.wsdl.Part;
import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;
import uni.ccm.toolitko.dto.SoapOperationDTO;
import uni.ccm.toolitko.parsing.GenericMarshaller;
import uni.ccm.toolitko.parsing.impl.SoapOperationMarshallerImpl;
import uni.ccm.toolitko.processing.util.SchemaMerger;
import uni.ccm.toolitko.utils.FileUtils;

public class OperationProcessor {

	private final static Logger log = Logger.getLogger(OperationProcessor.class);
	private FileUtils fileUtils;
	private Map<String, ComplexTypeDTO> typeMap;

	public List<SoapOperationDTO> process(Definitions inputWsdl, List<ComplexTypeDTO> types) {
		this.fileUtils = FileUtils.getInstance();
		this.typeMap = initializeTypeMap(types);
		List<SoapOperationDTO> operations = new ArrayList<SoapOperationDTO>();
		for (Operation soapOperation : inputWsdl.getOperations()) {
			operations.add(initializeOperation(soapOperation));
		}
		try {
			processOperations(operations);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return operations;
	}

	private void processOperations(List<SoapOperationDTO> operations) throws JAXBException, IOException {
		GenericMarshaller<SoapOperationDTO> marshaller = new SoapOperationMarshallerImpl()
				.setOutputFolder(AppSettings.outputRootDirectory);

		for (SoapOperationDTO operation : operations) {
			if (fileUtils.existsFileIgnoreExtension(AppSettings.outputRootDirectory, operation.getSoapOperationName())
					&& !AppSettings.forceOperationRebuild) {
				SoapOperationDTO oldVersion = marshaller.unmarshall(operation.getSoapOperationName());
				SoapOperationDTO merged = mergeOperations(operation, oldVersion);
				marshaller.marshall(merged);
			} else {
				marshaller.marshall(operation);
			}

		}

	}

	private SoapOperationDTO mergeOperations(SoapOperationDTO newV, SoapOperationDTO oldV) {
		Map<String, ComplexTypeDTO> newParts = initializeTypeMap(newV.getParts());
		Map<String, ComplexTypeDTO> oldParts = initializeTypeMap(oldV.getParts());
		ArrayList<ComplexTypeDTO> mergedParts = new ArrayList<ComplexTypeDTO>();
		mergedParts.add(SchemaMerger.mergeComplexTypes(newParts.get("input"), oldParts.get("input")));
		mergedParts.add(SchemaMerger.mergeComplexTypes(newParts.get("output"), oldParts.get("output")));
		oldV.setParts(mergedParts);
		return oldV;
	}

	private SoapOperationDTO initializeOperation(Operation soapOperation) {
		log.info(String.format("Processing operation %s", soapOperation.getName()));
		SoapOperationDTO operation = new SoapOperationDTO();
		operation.setSoapAction("");
		operation.setSoapOperationName(soapOperation.getName());
		operation.getParts().add(compileInput(soapOperation.getInput()));
		operation.getParts().add(compileOutput(soapOperation.getOutput()));
		return operation;
	}

	private ComplexTypeDTO compileInput(AbstractPortTypeMessage io) {
		return compileIO(io, "input");
	}

	private ComplexTypeDTO compileOutput(AbstractPortTypeMessage io) {
		return compileIO(io, "output");
	}

	private ComplexTypeDTO compileIO(AbstractPortTypeMessage io, String rootname) {
		ComplexTypeDTO messageRoot = new ComplexTypeDTO(rootname);
		List<ComplexTypeAttrDTO> messageParts = new ArrayList<ComplexTypeAttrDTO>();
		for (Part part : io.getMessage().getParts()) {
			messageParts.add(new ComplexTypeAttrDTO(part.getName(), part.getElement().getName()));
		}
		messageRoot.setAttributes(messageParts);
		compileObjectTree(messageRoot);

		return messageRoot;
	}

	private void compileObjectTree(ComplexTypeDTO type) {
		if (type != null && type.getAttributes() != null) {
			for (ComplexTypeAttrDTO attribute : type.getAttributes()) {
				if (attribute.isComplexType()) {
					ComplexTypeDTO nestedType = getTypeByNameOrEmpty(attribute.getType());
					attribute.setAttributes(nestedType.getAttributes());
					compileObjectTree(nestedType);
				}
			}
		}
	}

	private Map<String, ComplexTypeDTO> initializeTypeMap(List<ComplexTypeDTO> types) {
		Map<String, ComplexTypeDTO> typeMap = new HashMap<String, ComplexTypeDTO>();
		for (ComplexTypeDTO type : types) {
			typeMap.put(type.getName(), type);
		}
		return typeMap;
	}

	private ComplexTypeDTO getTypeByNameOrEmpty(String name) {
		if (this.typeMap.containsKey(name)) {
			return typeMap.get(name);
		} else {
			return new ComplexTypeDTO();
		}
	}
}
