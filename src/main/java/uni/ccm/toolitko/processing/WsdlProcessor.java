package uni.ccm.toolitko.processing;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;

import com.predic8.soamodel.Difference;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wsdl.diff.WsdlDiffGenerator;
import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeDTO;
import uni.ccm.toolitko.utils.FileUtils;

public class WsdlProcessor {

	static WSDLParser parser = new WSDLParser();
	private final static Logger log = Logger.getLogger(WsdlProcessor.class);
	private final static SchemaProcessor schemaProcessor = new SchemaProcessor();
	private final static OperationProcessor operationProcessor = new OperationProcessor();

	public static boolean startProcessing() {
		log.info("Processing started");

		Optional<List<String>> inputWsdls = getAllInputWsdlFiles();
		if (inputWsdls.isPresent()) {
			log.info(String.format("Found %d WSDL files to process.", inputWsdls.get().size()));
			for (String wsdlFile : inputWsdls.get()) {
				if (isChanged(wsdlFile) || AppSettings.forceOperationRebuild) {
					processWsdlFile(wsdlFile);
				} else {
					continue;
				}

			}
		} else {
			log.info("No input WSDL files to process.");
		}
		return true;
	}

	private static void processWsdlFile(String wsdlFile) {
		log.info(String.format("Starting to process file %s", wsdlFile));

		Definitions wsdlDefinition = parser.parse(AppSettings.inputDirectory + wsdlFile);

		log.info(String.format("Processing schemas"));
		List<ComplexTypeDTO> types = schemaProcessor.process(wsdlDefinition);

		log.info(String.format("Processing operations"));
		operationProcessor.process(wsdlDefinition, types);

		try {
			FileUtils.getInstance().copyTo(AppSettings.inputDirectory + wsdlFile, AppSettings.outputWsdlDirectory);
		} catch (IOException e) {
			log.error(String.format("Could not copy wsdl to output folder %s", AppSettings.outputWsdlDirectory));
			return;
		}
	}

	private static boolean isChanged(String newWsdl) {
		try {
			Definitions inputWsdl = parser.parse(AppSettings.inputDirectory + newWsdl);
			Definitions previousWsdl = parser.parse(AppSettings.outputWsdlDirectory + newWsdl);
			if (inputWsdl != null && previousWsdl != null) {
				WsdlDiffGenerator diffGen = new WsdlDiffGenerator(inputWsdl, previousWsdl);
				List<Difference> lst = diffGen.compare();
				if (lst.size() > 0) {
					log.info(String.format("The file %s is changed or new", newWsdl));
				} else {
					log.info(String.format("The file %s is identical with previous version", newWsdl));
				}
				return lst.size() > 0;
			}
		} catch (Exception e) {
			log.info("Output wsdl was not found.");
		}
		return true;
	}

	private static Optional<List<String>> getAllInputWsdlFiles() {
		Optional<List<String>> listOfOutFiles = Optional.empty();
		try {
			listOfOutFiles = Optional.of(FileUtils.getInstance().getXmlFilesInDirectory(AppSettings.inputDirectory));
		} catch (IOException e) {
			log.error(
					String.format("Could not retrieve any files from source directory %s", AppSettings.inputDirectory));
		}
		return listOfOutFiles;

	}

}
