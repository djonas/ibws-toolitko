package uni.ccm.toolitko.processing;

import java.util.List;
import java.util.Optional;
import javax.xml.bind.JAXBException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.predic8.schema.ComplexType;
import com.predic8.schema.Element;
import com.predic8.schema.Schema;
import com.predic8.wsdl.Definitions;
import static java.util.stream.Collectors.*;
import java.io.IOException;
import java.util.ArrayList;
import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;
import uni.ccm.toolitko.utils.FileUtils;
import uni.ccm.toolitko.utils.XmlUtils;
import uni.ccm.toolitko.parsing.*;
import uni.ccm.toolitko.parsing.impl.ComplexTypeMarshallerImpl;
import uni.ccm.toolitko.processing.util.SchemaMerger;

public class SchemaProcessor {
	
	private final static Logger log = Logger.getLogger(SchemaProcessor.class);
	
    private final static String ATTRIBUTE_NAME_NAME = "name";
    private final static String ATTRIBUTE_TYPE_NAME = "type";
    
    private FileUtils fileUtils;

	public List<ComplexTypeDTO> process(Definitions inputWsdl) {
		
		fileUtils = FileUtils.getInstance();
		List<ComplexTypeDTO> types = new ArrayList<ComplexTypeDTO>();
		for (Schema wsdlSchema : inputWsdl.getSchemas()) {
			try {
				types.addAll(initializeElements(wsdlSchema.getAllElements()));
				types.addAll(initializeComplexTypes(wsdlSchema.getComplexTypes()));
				log.info(String.format("Initialized %d objects", types.size()));
				types = processTypes(types);
			} catch (Exception e) {
				e.printStackTrace();
		
			}
		}
		return types;
	}

	private List<ComplexTypeDTO> initializeComplexTypes(List<ComplexType> complexTypes) {
		log.info("Processing all schema complex types");
		return complexTypes.stream().parallel().map(this::initializeComplexType).collect(toList());
	}

	private List<ComplexTypeDTO> initializeElements(List<Element> elements) {
		log.info("Processing all schema elements");
		return elements.stream().parallel().map(this::initializeElement).collect(toList());

	}

	private ComplexTypeDTO initializeComplexType(ComplexType complexType) {
		log.info(String.format("Processing complex type %S", complexType.getName()));
		Document typeXml = XmlUtils.parse(complexType.getAsString());
		return converXmlToDto(typeXml,false);
	}

	private ComplexTypeDTO initializeElement(Element complexType) {
		log.info(String.format("Processing element %S", complexType.getName()));
		Document typeXml = XmlUtils.parse(complexType.getAsString());
		return converXmlToDto(typeXml,true);
	}

	private ComplexTypeDTO converXmlToDto(Document xmlElement, boolean isElement) {
		ComplexTypeDTO ctDto = new ComplexTypeDTO(false);
		ctDto.setName(xmlElement.getDocumentElement().getAttribute(ATTRIBUTE_NAME_NAME));
		ctDto.setElement(isElement);
		Optional<List<ComplexTypeAttrDTO>> ctAttributes = getComplexTypeAttributes(xmlElement, ctDto.getName());
		if (ctAttributes.isPresent()) {
			ctDto.setAttributes(ctAttributes.get());
		}
		log.debug(String.format("Created complexType is %s", ctDto.toString()));
		return ctDto;
	}

	private Optional<List<ComplexTypeAttrDTO>> getComplexTypeAttributes(Document typeXml, String parentName) {
		Optional<List<ComplexTypeAttrDTO>> attributeList = Optional.empty();

		NodeList nodeList = typeXml.getElementsByTagName("*");

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE && node.getLocalName().equals("element")) {
				if (!attributeList.isPresent()) {
					attributeList = Optional.of(new ArrayList<ComplexTypeAttrDTO>());
				}
				String typeWithPrefix = XmlUtils.getNodeAttributeValue(node, ATTRIBUTE_TYPE_NAME);
				String type = (typeWithPrefix.contains(":"))? typeWithPrefix.split(":", 2)[1] : typeWithPrefix;
				String name = XmlUtils.getNodeAttributeValue(node, ATTRIBUTE_NAME_NAME);
				if (name != parentName) {
					attributeList.get().add(new ComplexTypeAttrDTO(name, type));
				}
			}
		}
		log.debug(String.format("Found %d attributes.", attributeList.isPresent() ? attributeList.get().size() : 0));
		return attributeList;
	}

	private List<ComplexTypeDTO> processTypes(List<ComplexTypeDTO> types) throws IOException, JAXBException {

		GenericMarshaller<ComplexTypeDTO> marshaller = new ComplexTypeMarshallerImpl().setOutputFolder(AppSettings.outputTypeDirectory);
		List<ComplexTypeDTO> processedTypes = new ArrayList<ComplexTypeDTO>();
		for (ComplexTypeDTO ctDto : types) {
			if(!ctDto.isElement()){
				if(fileUtils.existsFileIgnoreExtension(AppSettings.outputTypeDirectory, ctDto.getName()) && !AppSettings.forceTypeRebuild){
					log.info(String.format("File %s%s already exists, merging", AppSettings.outputTypeDirectory,ctDto.getName()));
					ComplexTypeDTO prevVersion = marshaller.unmarshall(ctDto.getName());
					if(!prevVersion.equals(ctDto)){
						ctDto = SchemaMerger.mergeComplexTypes(ctDto, prevVersion);
						marshaller.marshall(ctDto);			
					} else {
						ctDto = prevVersion;
					}
				} else {
					log.info(String.format("Creating file %s%s", AppSettings.outputTypeDirectory,ctDto.getName()));
					marshaller.marshall(ctDto);
					
				}
			}
			processedTypes.add(ctDto);
		}
		return processedTypes;
	}
}
