package uni.ccm.toolitko.parsing.validation;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.util.ValidationEventCollector;

import org.apache.log4j.Logger;

public class UniValidationEventCollector extends ValidationEventCollector {
	private final static Logger log = Logger.getLogger(UniValidationEventCollector.class);

	@Override
	public boolean handleEvent(ValidationEvent event) {
		super.handleEvent(event);
		log.error(event.getMessage(), event.getLinkedException());
		return true;
	}

}