package uni.ccm.toolitko.parsing;

import java.io.IOException;

import javax.xml.bind.JAXBException;

public abstract class GenericMarshaller<T> {

	public abstract void marshall(T t) throws IOException, JAXBException;

	public abstract T unmarshall(String xmlname) throws IOException, JAXBException;

	public abstract GenericMarshaller<T> setOutputFolder(String dir);
}
