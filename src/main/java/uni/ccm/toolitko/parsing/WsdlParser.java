package uni.ccm.toolitko.parsing;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.reficio.ws.SoapContext;
import org.reficio.ws.builder.SoapBuilder;
import org.reficio.ws.builder.SoapOperation;
import org.reficio.ws.builder.core.Wsdl;
import org.reficio.ws.common.ResourceUtils;

/**
 * Service for working with wsdl file and it's bindings and operations.
 * 
 * @author Jonáš Obdržálek
 *
 */
@Deprecated
public class WsdlParser {

	static Logger logger = Logger.getLogger(WsdlParser.class);
	static SoapBuilder builder;
	static SoapContext context = SoapContext.builder().alwaysBuildHeaders(true).exampleContent(false).typeComments(true)
			.buildOptional(true).valueComments(true).build();

	/**
	 * Return the wsdl object parsed from given wsdl name. The file needs to be
	 * in resources folder, otherwise is not found.
	 * 
	 * @param filename
	 *            Given filename of wsdl to be returned.
	 * @return
	 */
	@Deprecated
	public static Wsdl getWsdl(String filename) throws Exception {
		logger.debug(String.format("Getting wsdl for path wsdls/%s", filename));
		URL wsdlUrl = ResourceUtils.getResourceWithAbsolutePackagePath("/", "wsdls/" + filename);
		return Wsdl.parse(wsdlUrl);
	}

	/**
	 * Return the wsdl object parsed from given wsdl name. otherwise is not
	 * found.
	 * 
	 * @param filename
	 *            Given filename of wsdl to be returned.
	 * @return
	 */
	@Deprecated
	public static Wsdl getWsdl(String folder, String filename) throws Exception {
		logger.debug(String.format("Getting wsdl for path %s/%s", folder, filename));
		URL wsdlUrl = new URL("file:" + folder + filename);
		return Wsdl.parse(wsdlUrl);
	}

	/**
	 * Returns all SoapOperations available on one Wsdl.
	 * 
	 * @param wsdl
	 *            Object representation of parsed Wsdl file. See
	 *            {@link org.reficio.ws.builder.core.Wsdl}.
	 * @return
	 */
	@Deprecated
	public static List<SoapOperation> getAllOperations(Wsdl wsdl) {
		logger.debug(String.format("Getting all operations for wsdl"));
		List<SoapOperation> allOperations = new ArrayList<SoapOperation>();

		for (QName binding : getWsdlBindings(wsdl)) {
			builder = getBuilder(binding, wsdl);
			List<SoapOperation> bindingOperations = builder.getOperations();
			allOperations.addAll(bindingOperations);
		}

		return allOperations;
	}

	/**
	 * Generates the XML request of provided operation and wsdl.
	 * 
	 * @param wsdl
	 *            {@link Wsdl} to which the operation belongs. Needed for
	 *            correct builder initialization
	 * @param operation
	 *            {@link SoapOperation} for which the request is generated
	 * @return {@link String} representation of XML request
	 */
	@Deprecated
	public static String getXmlRequestMessage(Wsdl wsdl, SoapOperation operation) {
		logger.debug(String.format("Generating request soap message for operation %s", operation.getOperationName()));
		builder = getBuilder(operation.getBindingName(), wsdl);

		String requestMessage = builder.buildInputMessage(operation, context);
		logger.debug(String.format("The generated request: %s", requestMessage));

		return requestMessage;

	}

	/**
	 * Generates the XML response of provided operation and wsdl.
	 * 
	 * @param wsdl
	 *            {@link Wsdl} to which the operation belongs. Needed for
	 *            correct builder initialization
	 * @param operation
	 *            {@link SoapOperation} for which the response is generated
	 * @return {@link String} representation of XML response
	 */
	@Deprecated
	public static String getXmlResponseMessage(Wsdl wsdl, SoapOperation operation) {
		logger.debug(String.format("Generating response soap message for operation %s", operation.getOperationName()));
		builder = getBuilder(operation.getBindingName(), wsdl);

		String responseMessage = builder.buildInputMessage(operation, context);
		logger.debug(String.format("The generated response: %s", responseMessage));

		return responseMessage;

	}
	@Deprecated
	private static List<QName> getWsdlBindings(Wsdl wsdl) {
		return wsdl.getBindings();
	}
	@Deprecated
	private static SoapBuilder getBuilder(QName binding, Wsdl wsdl) {
		return wsdl.binding().localPart(binding.getLocalPart()).find();
	}

}
