package uni.ccm.toolitko.parsing.adapters;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;

/**
 * Custom JAXB adapter to handle marshalling Complex Type Attributes. The
 * marshalling ensures, that every node representing attribute will have name as
 * the name of the attribute in WSDL, instead of java object or JAXB defined
 * name. Unmarshalling is never called as it is redirected to
 * {@link ComplexTypeAdapter}}, because {@link ComplexTypeAttrDTO} is child to
 * {@link ComplexTypeDTO}
 * 
 * @author Jonáš Obdržálek
 *
 */
@SuppressWarnings("rawtypes")
public class ComplexTypeAttrAdapter extends XmlAdapter<Object, ComplexTypeAttrDTO> {

	/**
	 * Overriden marshal method of regular JAXB adapter.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public JAXBElement marshal(ComplexTypeAttrDTO ctAttrDto) throws Exception {
		JAXBElement<ComplexTypeAttrDTO> jaxbElement = new JAXBElement(new QName(ctAttrDto.getName()),
				ctAttrDto.getClass(), ctAttrDto);
		return jaxbElement;
	}

	/**
	 * Overriden unmarshal method of regular JAXB adapter.
	 */
	@Override
	public ComplexTypeAttrDTO unmarshal(Object v) throws Exception {
		return new ComplexTypeAttrDTO();
	}
}
