package uni.ccm.toolitko.parsing.adapters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;


import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;

/**
 * Custom JAXB adapter to handle marshalling and unmarshalling Complex Type
 * files and/or whole soap operation files. The marshalling ensures, that every
 * node representing complex type will have name as the name of the attribute in
 * WSDL, instead of java object or JAXB defined name. Unmarshalling then
 * recursively de-constructs complex type or operation back to the original data
 * transfer objects.
 * 
 * @author Jonáš Obdržálek
 *
 */
@SuppressWarnings("rawtypes")
public class ComplexTypeAdapter extends XmlAdapter<Object, ComplexTypeDTO> {
	private final static Logger log = Logger.getLogger(ComplexTypeAdapter.class);

	/**
	 * Overriden marshal method of regular JAXB adapter.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public JAXBElement marshal(ComplexTypeDTO ctDto) throws Exception {
		JAXBElement<ComplexTypeDTO> jaxbElement = new JAXBElement(new QName(ctDto.getName()), ctDto.getClass(), ctDto);
		return jaxbElement;
	}

	/**
	 * Overriden unmarshall method of regular JAXB adapter.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ComplexTypeDTO unmarshal(Object v) throws Exception {
		ComplexTypeDTO ctDto = new ComplexTypeDTO();

		if (v instanceof JAXBElement) {
			JAXBElement<ComplexTypeDTO> element = (JAXBElement<ComplexTypeDTO>) v;
			ctDto.setName(element.getName().getLocalPart());
		}
		if (v instanceof ElementNSImpl) {
			ElementNSImpl element = (ElementNSImpl) v;
			ctDto.setName(element.getNodeName());
			NodeList nodes = element.getChildNodes();
			ctDto.setAttributes(getAttributesFromNodes(nodes));

		}
		System.out.println(ctDto.toString());
		return ctDto;
	}

	/**
	 * Recursive method for re-creating the attributes tree.
	 * 
	 * @param nodes
	 * @return
	 */
	private List<ComplexTypeAttrDTO> getAttributesFromNodes(NodeList nodes) {
		List<ComplexTypeAttrDTO> attributes = new ArrayList<ComplexTypeAttrDTO>();
		for (int i = 0; nodes != null && i < nodes.getLength(); i++) {
			Node attrnode = nodes.item(i);
			log.debug(String.format("-Node %s is element %s and is param %s", attrnode.getLocalName(),
					attrnode.getNodeType() == Node.ELEMENT_NODE, isMetadataNode(attrnode)));
			if (attrnode.getNodeType() == Node.ELEMENT_NODE && !isMetadataNode(attrnode)) {
				ComplexTypeAttrDTO attr = new ComplexTypeAttrDTO();
				attr.setName(attrnode.getLocalName());
				List<ComplexTypeAttrDTO> nestedAttributes = new ArrayList<ComplexTypeAttrDTO>();
				NodeList subnodes = attrnode.getChildNodes();
				for (int j = 0; subnodes != null && j < subnodes.getLength(); j++) {
					if (subnodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
						if (subnodes.item(j).getNodeName().equals("metadata")) {
							resolveMetadataNodes(subnodes.item(j).getChildNodes(), attr);
						}
					} else {
						nestedAttributes = getAttributesFromNodes(subnodes);

					}
				}
				if (nestedAttributes.size() > 0) {
					attr.setAttributes(nestedAttributes);
				}
				if(attr.getName() != null && attr.getType() != null){
					initializeEmptyValues(attr);
					attributes.add(attr);
				}
			}
		}
		return attributes;
	}

	/**
	 * Because JAXB don't marshall to xml null strings, we need to initialize
	 * those that weren't found in source xml. Otherwise those nodes can be lost
	 * when the unmarhsalled object is marshalled back.
	 * 
	 * @param attr
	 *            {@link ComplexTypeAttrDTO} in which the empty values will be
	 *            initialized.
	 */
	private void initializeEmptyValues(ComplexTypeAttrDTO attr) {
		if (!attr.isComplexType()) {
			if (attr.getMetadata().getCcmName() == null) {
				attr.getMetadata().setCcmName("");
			}
			if (attr.getMetadata().getCodeList() == null) {
				attr.getMetadata().setCodeList("");
			}
			if (attr.getMetadata().getFormat() == null) {
				attr.getMetadata().setFormat("");
			}
		}
		if (attr.getMetadata().getRequired() == null) {
			attr.getMetadata().setRequired("");
		}
		if (attr.getMetadata().getDescription() == null) {
			attr.getMetadata().setDescription("");
		}
	}

	/**
	 * Evaluates DOM node and determines if it is node of metadata by name.
	 * 
	 * @param node
	 *            {@link Node} to be evaluated if it is metadata root or
	 *            attribute node
	 * @return Boolean, true if it is metadata node.
	 */
	private boolean isMetadataNode(Node node) {
		String name = node.getLocalName();
		if (name != null) {
			return Arrays.stream(AppSettings.attributeParams).parallel().anyMatch(name::equals);
		} else {
			return false;
		}
	}

	/**
	 * Iterates over {@link NodeList} which contains all metadata values.
	 * 
	 * @param metadatanodes
	 *            {@link NodeList} list of child nodes belonging to metadata
	 *            node.
	 * @param attr
	 *            {@link ComplexTypeAttrDTO} to which the metadata values will
	 *            be resolved.
	 */
	private void resolveMetadataNodes(NodeList metadatanodes, ComplexTypeAttrDTO attr) {
		for (int index = 0; metadatanodes != null && index < metadatanodes.getLength(); index++) {
			if (metadatanodes.item(index).getNodeType() == Node.ELEMENT_NODE) {
				resolveNodeValue(metadatanodes.item(index), attr);
			}

		}
	}

	/**
	 * Resolves if the input node is of some specific attribute by name of the
	 * node and the passes the value to the specific attribute of the input
	 * ComplexType metadata object.
	 * 
	 * @param node
	 *            {@link Node} from which the value will be resolved.
	 * @param attr
	 *            {@link ComplexTypeAttrDTO} to which the metadata value will be
	 *            resolved.
	 */
	private void resolveNodeValue(Node node, ComplexTypeAttrDTO attr) {
		if (node.getFirstChild() != null) {
			switch (node.getNodeName()) {
			case "type":
				attr.setType(node.getFirstChild().getNodeValue());
				attr.getMetadata().setType(node.getFirstChild().getNodeValue());
				break;
			case "format":
				attr.getMetadata().setFormat(node.getFirstChild().getNodeValue());
				break;
			case "maxLength":
				try {
					attr.getMetadata().setMaxLength(Integer.valueOf(node.getFirstChild().getNodeValue()));
				} catch (NumberFormatException e) {
					log.error("The attribute 'maxLength' in source XML is not a number");
				}
				break;
			case "description":
				attr.getMetadata().setDescription(node.getFirstChild().getNodeValue());
				break;
			case "codeList":
				attr.getMetadata().setCodeList(node.getFirstChild().getNodeValue());
				break;
			case "ccmName":
				attr.getMetadata().setCcmName(node.getFirstChild().getNodeValue());
				break;
			case "required":
				attr.getMetadata().setRequired(node.getFirstChild().getNodeValue());
				break;
			default:
				break;
			}
		}
	}
}
