package uni.ccm.toolitko.parsing.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.transform.stream.StreamSource;

import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;
import uni.ccm.toolitko.dto.util.ObjectWrapper;
import uni.ccm.toolitko.parsing.GenericMarshaller;
import uni.ccm.toolitko.parsing.validation.UniValidationEventCollector;
import uni.ccm.toolitko.utils.FileUtils;

public class ComplexTypeMarshallerImpl extends GenericMarshaller<ComplexTypeDTO> {

	private FileUtils fileUtils;
	private JAXBContext jaxbContext;
	private ValidationEventCollector validationEventCollector;
	private Marshaller jaxbMarshaller;
	private Unmarshaller jaxbUnmarshaller;

	private String outputFolder;

	public ComplexTypeMarshallerImpl() throws JAXBException {
		this.fileUtils = FileUtils.getInstance();
		this.jaxbContext = JAXBContext.newInstance(ObjectWrapper.class, ComplexTypeDTO.class, ComplexTypeAttrDTO.class);
		this.validationEventCollector = new UniValidationEventCollector();
		this.outputFolder = AppSettings.outputTypeDirectory;
	}

	@Override
	public void marshall(ComplexTypeDTO type) throws IOException, JAXBException {
		File typeFile = fileUtils.createNewFile(outputFolder, type.getName(), "xml");
		ObjectWrapper wrappedDto = new ObjectWrapper(type);
		jaxbMarshaller = ensureMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setEventHandler(validationEventCollector);
		jaxbMarshaller.marshal(wrappedDto, typeFile);
	}

	@Override
	public ComplexTypeDTO unmarshall(String type) throws IOException, JAXBException {
		File file = new File(outputFolder + type + ".xml");
		jaxbUnmarshaller = ensureUnmarshaller();
		jaxbUnmarshaller.setEventHandler(validationEventCollector);
		JAXBElement<ObjectWrapper> root = jaxbUnmarshaller.unmarshal(new StreamSource(new FileInputStream(file)),
				ObjectWrapper.class);
		ObjectWrapper wrapper = root.getValue();
		return wrapper.getObject();
	}

	private Unmarshaller ensureUnmarshaller() throws JAXBException {
		if (this.jaxbUnmarshaller == null) {
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		}
		return this.jaxbUnmarshaller;
	}

	private Marshaller ensureMarshaller() throws JAXBException {
		if (this.jaxbMarshaller == null) {
			jaxbMarshaller = jaxbContext.createMarshaller();
		}
		return this.jaxbMarshaller;
	}

	@Override
	public GenericMarshaller<ComplexTypeDTO> setOutputFolder(String path) {
		this.outputFolder = path;
		return this;
	}

}
