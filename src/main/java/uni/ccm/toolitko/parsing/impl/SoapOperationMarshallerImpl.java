package uni.ccm.toolitko.parsing.impl;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.ValidationEventCollector;
import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.dto.ComplexTypeAttrDTO;
import uni.ccm.toolitko.dto.ComplexTypeDTO;
import uni.ccm.toolitko.dto.SoapOperationDTO;
import uni.ccm.toolitko.parsing.GenericMarshaller;
import uni.ccm.toolitko.parsing.validation.UniValidationEventCollector;
import uni.ccm.toolitko.utils.FileUtils;

public class SoapOperationMarshallerImpl extends GenericMarshaller<SoapOperationDTO> {

	private FileUtils fileUtils;
	private JAXBContext jaxbContext;
	private ValidationEventCollector validationEventCollector;
	private Marshaller jaxbMarshaller;
	private Unmarshaller jaxbUnmarshaller;

	private String outputFolder;

	public SoapOperationMarshallerImpl() throws JAXBException {
		this.fileUtils = FileUtils.getInstance();
		this.jaxbContext = JAXBContext.newInstance(SoapOperationDTO.class, ComplexTypeDTO.class,
				ComplexTypeAttrDTO.class);
		this.validationEventCollector = new UniValidationEventCollector();
		this.outputFolder = AppSettings.outputRootDirectory;
	}

	@Override
	public void marshall(SoapOperationDTO operation) throws IOException, JAXBException {
		File typeFile = fileUtils.createNewFile(outputFolder, operation.getSoapOperationName(), "xml");
		jaxbMarshaller = ensureMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.setEventHandler(validationEventCollector);
		jaxbMarshaller.marshal(operation, typeFile);

	}

	@Override
	public SoapOperationDTO unmarshall(String operation) throws IOException, JAXBException {
		File file = new File(outputFolder + operation + ".xml");
		jaxbUnmarshaller = ensureUnmarshaller();
		jaxbUnmarshaller.setEventHandler(validationEventCollector);
		SoapOperationDTO root = (SoapOperationDTO) jaxbUnmarshaller.unmarshal(file);
		// oapOperationDTO unmarshalledOperation = root.getValue();
		return root;
	}

	private Unmarshaller ensureUnmarshaller() throws JAXBException {
		if (this.jaxbUnmarshaller == null) {
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		}
		return this.jaxbUnmarshaller;
	}

	private Marshaller ensureMarshaller() throws JAXBException {
		if (this.jaxbMarshaller == null) {
			jaxbMarshaller = jaxbContext.createMarshaller();
		}
		return this.jaxbMarshaller;
	}

	@Override
	public GenericMarshaller<SoapOperationDTO> setOutputFolder(String path) {
		this.outputFolder = path;
		return this;
	}

}
