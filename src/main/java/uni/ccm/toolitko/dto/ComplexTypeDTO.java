package uni.ccm.toolitko.dto;

import java.util.List;
import java.util.TreeSet;
import javax.xml.bind.annotation.adapters.*;
import uni.ccm.toolitko.parsing.adapters.ComplexTypeAdapter;
import javax.xml.bind.annotation.*;

/**
 * Object representing complex type and/or element parsed from WSDL file.
 * @author Jonáš Obdržálek
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ ComplexTypeAttrDTO.class })
@XmlJavaTypeAdapter(ComplexTypeAdapter.class)
public class ComplexTypeDTO {

	@XmlTransient
	private String name;

	@XmlTransient
	private boolean isElement;

	@XmlMixed
	@XmlAnyElement(lax = true)
	private List<ComplexTypeAttrDTO> attributes;

	public ComplexTypeDTO(boolean isElement) {
		super();
		this.isElement = isElement;
	}

	public ComplexTypeDTO(String name) {
		super();
		this.name = name;
	}

	public ComplexTypeDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElementDecl(name = "")
	public List<ComplexTypeAttrDTO> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<ComplexTypeAttrDTO> attributes) {
		this.attributes = attributes;
	}

	public boolean isElement() {
		return isElement;
	}

	public void setElement(boolean isElement) {
		this.isElement = isElement;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ComplexType name: " + this.name);
		if (attributes != null) {
			for (ComplexTypeAttrDTO attrDto : attributes) {
				builder.append(attrDto.toString());
			}
		}
		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ComplexTypeDTO) {
			ComplexTypeDTO that = (ComplexTypeDTO) obj;
			if (!this.name.equals((that.getName()))) {
				return false;
			}
			TreeSet<ComplexTypeAttrDTO> thisAttrs = new TreeSet<ComplexTypeAttrDTO>(this.attributes);
			TreeSet<ComplexTypeAttrDTO> thatAttrs = new TreeSet<ComplexTypeAttrDTO>(that.getAttributes());
			if (!thisAttrs.equals(thatAttrs)) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

}
