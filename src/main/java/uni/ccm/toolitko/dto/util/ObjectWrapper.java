package uni.ccm.toolitko.dto.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import uni.ccm.toolitko.dto.ComplexTypeDTO;

/**
 * Data transfer object for correct unmarshalling of single complex type. The
 * complex type is wrapped in this object.
 * 
 * @author Jonáš Obdržálek
 *
 */
@XmlRootElement(name = "complexType")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ ComplexTypeDTO.class })
public class ObjectWrapper {

	/**
	 * Complex type to be wrapped inside this object.
	 */
	@XmlMixed
	@XmlAnyElement(lax = true)
	public ComplexTypeDTO object;

	public ObjectWrapper() {
		super();
	}

	public ObjectWrapper(ComplexTypeDTO object) {
		this.object = object;
	}

	public ComplexTypeDTO getObject() {
		return object;
	}

	public void setObject(ComplexTypeDTO object) {
		this.object = object;
	}

}
