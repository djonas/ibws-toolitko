package uni.ccm.toolitko.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Wrapper object for complex and single type metadata.
 * 
 * @author Jonáš Obdržálek
 *
 */
@XmlRootElement(name = "metadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class AttrMetadataDTO {
	/**
	 * Defines how long the value of attribute can be
	 */
	@XmlElement(required = true)
	private int maxLength;

	/**
	 * Miscellaneous description of the attribute.
	 */
	@XmlElement(required = true)
	private String description;

	/**
	 * If values of the attribute must comply to list of values, this specifies
	 * the code of said list.
	 */
	@XmlElement(required = true)
	private String codeList;

	/**
	 * If value of the attribute must be of some format (like date,...)
	 */
	@XmlElement(required = true)
	private String format;

	/**
	 * Specifies the type of the attribute.
	 */
	@XmlElement(required = true)
	private String type;

	/**
	 * The name of the attribute as seen in CCM application
	 */
	@XmlElement(required = true)
	private String ccmName;

	/**
	 * Contains information whether the attribute is required or not and by
	 * which rules.
	 */
	@XmlElement(required = true)
	private String required;

	public AttrMetadataDTO() {

	}

	public AttrMetadataDTO(String type) {
		this.type = type;
		this.ccmName = "";
		this.description = "";
		this.format = "";
		this.required = "";
		this.codeList = "";
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCodeList() {
		return codeList;
	}

	public void setCodeList(String codeList) {
		this.codeList = codeList;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCcmName() {
		return ccmName;
	}

	public void setCcmName(String ccmName) {
		this.ccmName = ccmName;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("\n-- metadata: ccmName %s", this.ccmName));
		sb.append(String.format("\n-- metadata: required %s", this.required));
		sb.append(String.format("\n-- metadata: description %s", this.description));
		sb.append(String.format("\n-- metadata: format %s", this.format));
		sb.append(String.format("\n-- metadata: codelist %s", this.codeList));
		sb.append(String.format("\n-- metadata: maxlength %d", this.maxLength));
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		try {
			if (!(obj instanceof AttrMetadataDTO)) {
				return false;
			} else {
				AttrMetadataDTO that = (AttrMetadataDTO) obj;
				if ((this.getCcmName() == null && that.codeList != null) || !this.getCcmName().equals(that.getCcmName())) {
					return false;
				}
				if ((this.getCodeList() == null && that.codeList != null) || !this.getCodeList().equals(that.getCodeList())) {
					return false;
				}
				if ((this.getDescription() == null && that.getDescription() != null ) || !this.getDescription().equals(that.getDescription())) {
					return false;
				}
				if ((this.getFormat() == null && that.getFormat() != null) || !this.getFormat().equals(that.getFormat())) {
					return false;
				}
				if ((this.getRequired() == null && that.getFormat() != null) || !this.getRequired().equals(that.getRequired())) {
					return false;
				}
			}
        } catch (ClassCastException unused)   {
            return false;
        } catch (NullPointerException unused) {
            return false;
        }
		return true;	
	}
}
