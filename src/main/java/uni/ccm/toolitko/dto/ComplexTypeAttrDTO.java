package uni.ccm.toolitko.dto;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

import javax.xml.bind.annotation.*;

import uni.ccm.toolitko.AppSettings;
import uni.ccm.toolitko.parsing.adapters.ComplexTypeAttrAdapter;

/**
 * Object representing complex type and/or element attribute parsed from WSDL
 * file. This attribute can be complex type itself, therefore creating tree
 * structure of the whole complex type.
 * 
 * @author Jonáš Obdržálek
 *
 */
@XmlJavaTypeAdapter(ComplexTypeAttrAdapter.class)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlType(propOrder = { "metadata" })
public class ComplexTypeAttrDTO extends ComplexTypeDTO implements Comparable<ComplexTypeAttrDTO> {

	@XmlTransient
	private boolean isComplexType;

	@XmlTransient
	private boolean isArray;

	@XmlTransient
	private String name;

	@XmlTransient
	private String type;

	@XmlElement(name = "metadata")
	private AttrMetadataDTO metadata;

	public ComplexTypeAttrDTO() {
		this.metadata = new AttrMetadataDTO();
	}

	public ComplexTypeAttrDTO(String name, String type) {
		this.metadata = new AttrMetadataDTO(type);
		this.name = name;
		this.type = type;
		this.isComplexType = isAttributeComplex(type);
		this.isArray = isAttributeArray(type);
	}

	public boolean isComplexType() {
		return isComplexType;
	}

	public boolean isArray() {
		return isArray;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		this.metadata.setType(type);
		this.isArray = isAttributeArray(type);
		this.isComplexType = isAttributeComplex(type);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private boolean isAttributeComplex(String attributeName) {
		if (attributeName == null || attributeName.equals(StringUtils.EMPTY)) {
			return false;
		}
		return !Arrays.stream(AppSettings.simpleTypes).parallel().anyMatch(attributeName::equals);

	}

	private boolean isAttributeArray(String type) {
		if (type == null || type.equals(StringUtils.EMPTY)) {
			return false;
		}
		return type.contains("Array");
	}

	public AttrMetadataDTO getMetadata() {
		return metadata;
	}

	public void setMetadata(AttrMetadataDTO metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n-Attribute name: " + this.name);
		builder.append("\n-Attribute type: " + this.type);
		builder.append("\n-Attribute is complex: " + this.isComplexType);
		builder.append("\n-Attribute is array: " + this.isArray);
		builder.append(this.metadata.toString());
		if (getAttributes() != null) {
			for (ComplexTypeAttrDTO attrDto : getAttributes()) {
				builder.append(attrDto.toString());
			}
		}
		return builder.toString();
	}

	@Override
	public int compareTo(ComplexTypeAttrDTO that) {
		return this.name.compareTo(that.getName());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ComplexTypeAttrDTO)) {
			return false;
		} else {
			ComplexTypeAttrDTO that = (ComplexTypeAttrDTO) obj;
			if (!this.getName().equals(that.getName())) {
				return false;
			}
			if (!this.getType().equals(that.getType())) {
				return false;
			}
		}
		return true;
	}
}
