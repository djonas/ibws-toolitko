package uni.ccm.toolitko.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * Data transfer object representing the SOAP operation parsed from WSDL file.
 * The parts will contain on two complex types, the input and output of the
 * operation.
 * 
 * @author Jonáš Obdržálek
 *
 */
@XmlRootElement(name = "soapOperation")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ ComplexTypeDTO.class })
@XmlType(propOrder = { "soapOperationName", "soapAction", "parts" })
public class SoapOperationDTO {

	private String soapOperationName;
	private String soapAction;

	@XmlMixed
	@XmlAnyElement(lax = true)
	private List<ComplexTypeDTO> parts;

	public String getSoapOperationName() {
		return soapOperationName;
	}

	public void setSoapOperationName(String soapOperationName) {
		this.soapOperationName = soapOperationName;
	}

	public List<ComplexTypeDTO> getParts() {
		if (this.parts == null) {
			this.parts = new ArrayList<ComplexTypeDTO>();
		}
		return this.parts;
	}

	public void setParts(List<ComplexTypeDTO> parts) {
		this.parts = parts;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n ---- Operation : " + this.getSoapOperationName());
		if (parts != null) {
			for (ComplexTypeDTO ct : parts) {
				sb.append("\n" + ct.toString());
			}
		}
		return sb.toString();
	}

}
