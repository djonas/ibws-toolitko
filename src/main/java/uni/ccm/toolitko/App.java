package uni.ccm.toolitko;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import uni.ccm.toolitko.processing.WsdlProcessor;

/**
 * Main class of IBWS-tool
 * 
 * @author Jonáš Obdržálek
 *
 */
public class App {
	private final static Logger log = Logger.getLogger(App.class);

	/**
	 * Main method. Runs as console application
	 * 
	 * @param args
	 *            List of arguments configuring the application behaviour:</br>
	 *            <b>inputDirectory=${path}</b> - Sets the directory where all
	 *            xml wsdl files will be located</br>
	 *            <b>outputDirectory=${path}</b> - Sets the directory where the
	 *            output is generated</br>
	 *            <b>forceTypeRebuild</b> - If present, the application will
	 *            ignore when there are no changes to complex types and rebuilds
	 *            every type from wsdl</br>
	 *            <b>forceOperationRebuild</b> - If present, the application
	 *            will ignore when there are no changes to the operations and
	 *            rebuilds every operation from the wsdl file</br>
	 */
	public static void main(String[] args) {
		log.info(String.format("Starting application with parameter %s", args.toString()));

		try {
			processArguments(args);
			if (argumentsSatisfied()) {
				WsdlProcessor.startProcessing();
			} else {
				throw new IllegalArgumentException("One or more required arguments are missing!");
			}
		} catch (Exception e) {
			log.error(String.format("Help, something bad happened: %s", e.getMessage()));
		}
	}

	/**
	 * Utility method for resolving all the application arguments and then
	 * storing them to the {@link AppSettings} class, containing all the
	 * application configuration.
	 * 
	 * @param args
	 *            String array of arguments. Those can be in plain string or in
	 *            format ${arg_name}=${arg_value}.
	 * 
	 */
	private static void processArguments(String[] args) {
		for (String argument : args) {
			String[] nvp = null;
			if (argument.contains("=")) {
				nvp = argument.split("=", 2);
			}
			switch ((nvp != null) ? nvp[0] : argument) {
			case AppSettings.arg_inputDirectory:
				AppSettings.inputDirectory = nvp[1];
				log.info(String.format("The source directory is set as %s", AppSettings.inputDirectory));
				break;
			case AppSettings.arg_outputDirectory:
				AppSettings.outputRootDirectory = nvp[1];
				AppSettings.outputWsdlDirectory = nvp[1] + "wsdls\\";
				AppSettings.outputTypeDirectory = nvp[1] + "types\\";
				log.info(String.format("The output root directory is set as %s", AppSettings.outputRootDirectory));
				break;
			case AppSettings.arg_forceTypeRebuild:
				AppSettings.forceTypeRebuild = true;
				log.info(String.format("Application will rebuild all type files."));
				break;
			case AppSettings.arg_forceOperationRebuild:
				AppSettings.forceOperationRebuild = true;
				log.info(String.format("Application will rebuild all operation files."));
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Utility method validating if all required arguments are satisfied.
	 * 
	 * @return Boolean, which is true if there is every required parameter
	 *         satisfied. If not, the application cannot work properly, thus
	 *         should throw an Exception.
	 * 
	 */
	private static boolean argumentsSatisfied() {
		if (StringUtils.EMPTY.equals(AppSettings.outputRootDirectory)) {
			return false;
		}
		if (StringUtils.EMPTY.equals(AppSettings.inputDirectory)) {
			return false;
		}
		return true;
	}
}
