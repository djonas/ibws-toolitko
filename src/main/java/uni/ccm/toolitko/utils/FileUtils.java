package uni.ccm.toolitko.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import uni.ccm.toolitko.AppSettings;

public final class FileUtils {

	private static FileUtils instance;

	public static synchronized FileUtils getInstance() {
		if (instance == null)
			instance = new FileUtils();
		return instance;
	}

	public List<String> getXmlFilesInResourcesDirectory(String path) throws IOException {
		List<String> filenames = new ArrayList<>();

		try (InputStream in = getResourceAsStream(path);
				BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
			String resource;

			while ((resource = br.readLine()) != null) {
				filenames.add(resource);
			}
		}

		return filenames;

	}

	public List<String> getXmlFilesInDirectory(String path) throws IOException {
		List<String> files = new ArrayList<String>();
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
			if (file.isFile() && isXml(file)) {
				files.add(file.getName());
			}
		}
		return files;
	}

	public void WriteToFile() {

	}

	public File getFile(String path) {

		return new File(path);
	}

	public boolean fileExists(String path) {
		return false;
	}

	private InputStream getResourceAsStream(String resource) {
		final InputStream in = getContextClassLoader().getResourceAsStream(resource);

		return in == null ? getClass().getResourceAsStream(resource) : in;
	}

	private ClassLoader getContextClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	private boolean isXml(File file) {
		String fileExtension = FilenameUtils.getExtension(file.getName());
		return Arrays.stream(AppSettings.xmlExtensions).parallel().anyMatch(fileExtension::contains);

	}

	public void copyTo(String fullPath, String toDir) throws IOException {
		org.apache.commons.io.FileUtils.copyFileToDirectory(new File(fullPath), new File(toDir));
	}

	public File createNewFile(String dir, String name, String extension) throws IOException {
		File file = new File(dir + name + "." + extension);
		file.getParentFile().mkdirs();
		file.createNewFile();
		return file;
	}

	public boolean existsFileIgnoreExtension(String dir, String name) throws IOException {
		File folder = new File(dir);
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles != null) {
			for (File file : listOfFiles) {
				if (file.isFile()) {
					String[] filename = file.getName().split("\\.(?=[^\\.]+$)");
					if (filename[0].equalsIgnoreCase(name))
						return true;
				}
			}
		}
		return false;
	}
}
