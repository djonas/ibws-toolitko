### How do I get set up? ###

1. Import into IDE of your choice.
2. In pom.xml is configuration, specify input and output folder.
3. Run maven with goal "exec:java -e"
4. ....
5. Profit.

**FYI: Je to hroznej kód, stydím se za to ... ale funguje to**, nepřepisuje to věci ve vygenerovaných XML pro každou soapOperation, pouze to odstraní nebo doplní prvky dle toho, jestli v novém wsdl chybí nebo přebývají. Taky to hlídá, jestli je daný atribut stejného typu, pokud není tak ho taky přepíše.